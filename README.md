# README #

This project is an example of how finger tracking can be used to create an application for car showing in Unity.


### What is this repository for? ###

Use a single finger to select the color of the car.

The example is written with the MetaSDK, but can be easily changed to use any finger tracking system in Unity.
Moving the trigger object named "Input" in the scene view while the game is running can simulate finger input before connecting your desired SDK.


### How do I get set up? ###

* Open project in Unity5
* Connect your finger tracking SDK to InputCollider.cs
* If you do not wish to use Meta, you may remove the library reference & and functions requiring it. You may also remove the Meta Asset from the Project.
* You may turn off the renderer for the cube parented to the input game object.

## Running without finger tracking ##

* Run the scene
* Move the input cube around in the scene view.

## How it works ##

* The Input gameObject is be stuck to your finger.
* The Color picker input elements respond to a collision of the input game object


### Who do I talk to? ###

* Repo owner or admin - Brennan Hatton  - brennan@siliconvagabond.com