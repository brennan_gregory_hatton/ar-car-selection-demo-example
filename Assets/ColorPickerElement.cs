﻿using UnityEngine;
using System.Collections;

public class ColorPickerElement : MonoBehaviour {
	
	//<summary>
	//This is the material that will be selected
	//</summary>
	public Material SelectMaterial;
	
	//<summary>
	//This is a reference to the cotroller that sotres the currently sleected value. This is the parent object and will be defined on start
	//</summary>
	ColorPickerController colorPickerController;
	
	//ID set by colorPickerController
	public int id;
	
	// Use this for initialization
	void Start () {
		
		//Get a reference to the parnet object
		colorPickerController = this.transform.parent.gameObject.GetComponent<ColorPickerController>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter ()
	{
		
		Debug.Log("Trigger");
		//Seleted is true
		Select();
	}
	
	void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision");
		//Seleted is true
		Select();
		
	}
	
	void Select()
	{
		//Set highlight
		Highlight();
		
		//Send message to ColorPickerController
		colorPickerController.Select(SelectMaterial, id);
	}
	
	public void Deselect()
	{
		//Remove highlight
		RemoveHighlight();
	}
	
	void Highlight()
	{
		this.transform.localScale = Vector3.one * 0.06f;
	}
	
	void RemoveHighlight()
	{
		this.transform.localScale = Vector3.one * 0.05f;
	}
}
