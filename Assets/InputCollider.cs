﻿// Use hand tracking system to stick this object to your finger / palm

using UnityEngine;
using System.Collections;
using Meta;

public class InputCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Meta.Hands.left.pointer.position != null && !float.IsNaN(Meta.Hands.left.pointer.position.x) && !float.IsNaN(Meta.Hands.left.pointer.position.y) && !float.IsNaN(Meta.Hands.left.pointer.position.z))
			this.transform.position = Meta.Hands.left.pointer.position;
	}
}
