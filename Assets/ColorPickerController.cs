﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorPickerController : MonoBehaviour {
	
	public Renderer targetRenderer;
	
	Material currentMaterial;
	
	List<ColorPickerElement> Elements = new List<ColorPickerElement>();
	
	// Use this for initialization
	void Start () {
		
		
		//Set children ID's
		ColorPickerElement tempElement;
		for(int i = 0; i < this.transform.childCount; i++)
		{
			tempElement = this.transform.GetChild(i).gameObject.GetComponent<ColorPickerElement>();
			tempElement.id = i;
			
			Elements.Add(tempElement);
		}
	}
	
	public void Select(Material material, int id)
	{
		
		//Update Material
		currentMaterial = material;
		
		//Apply to car
		targetRenderer.material = material;
		
		//deselect other colors
		//For all elements
		for(int i = 0; i < Elements.Count; i++)
		{
			//Check it is not the element we just selected
			if (Elements[i].id != id)
			{
				//Deselect the element
				Elements[i].Deselect();
			}
		}
		
		
	}
}
